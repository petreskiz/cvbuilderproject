import React, { Component } from "react";
import "./Tips.css";

class Tips extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return <div className="Tips"><p>{this.props.tip}</p></div>;
  }
}

export default Tips;
