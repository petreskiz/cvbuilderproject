import React, { Component } from "react";
import "./Portfolio.css";

class Portfolio extends Component {
  state = {};
  render() {
    return (
      <div className="Portfolio">
        <img
          src={require("../../images/Laika1.png")}
          alt=""
          style={{ marginTop: "50px" }}
          width="100%"
        />
        <img src={require("../../images/Laika2.png")} alt="" width="100%" />
        <img src={require("../../images/laika3.png")} alt="" width="100%" />
        <img src={require("../../images/Laika4.png")} alt="" width="100%" />
        <img src={require("../../images/Laika5.png")} alt="" width="100%" />
      </div>
    );
  }
}

export default Portfolio;
