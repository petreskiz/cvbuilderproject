import React from "react";
import "./WebAch.css";

const WebAch = props => {
  return (
    <div className="WebAch">
      <h3
        className="Title"
        suppressContentEditableWarning="true"
        onBlur={e =>
          props.updateFields(
            e.target.id,
            e.target.innerText,
            props.parentName,
            props.parentIndex
          )
        }
        contentEditable={props.toggle ? "true" : "false"}
        id="title"
      >
        {props.achivments.title}
      </h3>
      <p
        className="Description"
        suppressContentEditableWarning="true"
        onBlur={e =>
          props.updateFields(
            e.target.id,
            e.target.innerText,
            props.parentName,
            props.parentIndex
          )
        }
        contentEditable={props.toggle ? "true" : "false"}
        id="description"
      >
        {props.achivments.description}
      </p>
    </div>
  );
};

export default WebAch;
