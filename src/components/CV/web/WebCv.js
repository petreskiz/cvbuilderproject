import React, { Component } from "react";
import "./WebCv.css";
import WebCvProfile from "./WebCvProfile";
import WebCvWorkExp from "./WebCvWorkExp";
import WebSkills from "./WebSkills";
import WebAch from "./WebAch";
import WebInterests from "./WebInterests";
import WebEducation from "./WebEducation";

class WebCV extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: false,
      cv: {
        profile: {
          name: "Elon Musk",
          jobs: "Entrepreneur, Engineer, Inventor and Investor",
          email: "elon@teslamotors.com",
          city: "Los Angeles, USA",
          phone: "000-000-000",
          twitter: "@elonmusk",
          paragraph:
            "Aiming to reduce global warming through sustain-aible energy production and consuption. Planning to reduce the risk of human extinciton by making life multi-planetary and setting up ahuman colony on Mars"
        },
        workExperiences: [
          {
            isPresent: false,
            position: "Founder,CEO & Lead Designer",
            company: "SpaceX-Space Exploration Technologies",
            cityOfWork: "Los Angeles,Usa",
            startDate: "2002-06",
            endDate: "2003-06",
            accomplishments: [
              "tus, nihil mollitia porro rem! Ullam quis tenetur maxime alias mollitiaLorem ipsum dolor sit, amet consectetur adipisicing elit. Facere quis in odio repudiandae, id maiores vel iste cumque voluptas .",
              "repudiandae, id maiores vel iste cumque volupta",
              "repudiandae, id maiores vel iste cumque volupta"
            ]
          },
          {
            isPresent: true,
            position: "Lead Designer",
            company: "PayPal",
            cityOfWork: "Los Angeles,Usa",
            startDate: "2002-06",
            endDate: "2002-06",
            accomplishments: [
              "tus, nihil mollitia porro rem! Ullam quis tenetur maxime alias mollitiaLorem ipsum dolor sit, amet consectetur adipisicing elit. Facere quis in odio repudiandae, id maiores vel iste cumque voluptas .",
              "repudiandae, id maiores vel iste cumque volupta",
              "repudiandae, id maiores vel iste cumque volupta"
            ]
          }
        ],
        achivments: [
          {
            title: "IEEE Honorary Membership (2015)",
            description:
              "Given to people who have rendered maritorius service to humanity"
          },
          {
            title: "53rd Richest person in the world - Forbes (2018)"
          }
        ],
        educations: [
          {
            isPresent: false,
            institution: "Brainster/ss cyril and methodius university",
            program: "Data science",
            startDate: "2018-10",
            endDate: "2019-10",
            courses: ["Lead Generation"]
          }
        ],
        skills: [
          "Marketing",
          "Goal oriented",
          "Creatyvity",
          "Future focused",
          "Marketing",
          "Long-term thinking",
          "Leadership"
        ],
        interests: ["Physics", "Video Games", "AI", "Reading"]
      }
    };
  }
  onkeyup = (
    targetId,
    targetValue,
    parentName,
    parentIndex,
    index,
    arrayName
  ) => {
    if (index != undefined) {
      const { cv } = this.state;
      cv[parentName][parentIndex][arrayName][index] = targetValue;
      this.setState({
        cv
      });
    } else if (parentIndex != undefined) {
      const { cv } = this.state;
      cv[parentName][parentIndex][targetId] = targetValue;
      this.setState({
        cv
      });
    } else {
      const { cv } = this.state;
      cv[parentName][targetId] = targetValue;
      this.setState({
        cv
      });
    }
  };

  addExpirience = () => {};

  render() {
    return (
      <div className="CV-inside">
        <div className="Details">
          <WebCvProfile
            tip2={this.props.tip1}
            toggle={this.props.toggle}
            profile={this.state.cv.profile}
            updateFields={this.onkeyup}
            parentName="profile"
          />
          <hr
            style={{
              backgroundColor: "turquoise",
              border: "1px solid turquoise"
            }}
          />
          <div className="AllDetailsExperience">
            <div className="WorkExperience">
              <h3
                className="TitlesForCvExp"
                id="workexp"
                onMouseOver={e => this.props.tip1(`webcv${e.target.id}`)}
              >
                Work Exprerience
              </h3>
              {/* <button onClick={() => this.addExpirience}>Add Expirience</button> */}
              {this.state.cv.workExperiences.map((wp, i) => (
                <WebCvWorkExp
                  key={i}
                  tip2={this.props.tip1}
                  toggle={this.props.toggle}
                  experience={wp}
                  updateFields={this.onkeyup}
                  parentIndex={i}
                  parentName="workExperiences"
                />
              ))}

              <div>
                <h3 className="TitlesForCvExp"
                id="edu"
                onMouseOver={e => this.props.tip1(`webcv${e.target.id}`)}
                >Education</h3>
                {this.state.cv.educations.map((edu, i) => (
                  <WebEducation
                    key={i}
                    tip2={this.props.tip1}
                    toggle={this.props.toggle}
                    education={edu}
                    updateFields={this.onkeyup}
                    parentIndex={i}
                    parentName="educations"
                  />
                ))}
              </div>
            </div>
            <div className="Skills">
              <div>
                <h3
                  className="TitlesForCvExp"
                  id="skills"
                  onMouseOver={e => this.props.tip1(`webcv${e.target.id}`)}
                >
                  Skills & competencies
                </h3>
                <WebSkills
                  skills={this.state.cv.skills}
                  toggle={this.props.toggle}
                  updateFields={this.onkeyup}
                  parentName="skills"
                />
              </div>
              <div>
                <h3 className="TitlesForCvExp"
                 id="achv"
                 onMouseOver={e => this.props.tip1(`webcv${e.target.id}`)}
                >Achievments & Certificates</h3>
                {this.state.cv.achivments.map((ach, i) => (
                  <WebAch
                    key={i}
                    toggle={this.props.toggle}
                    achivments={ach}
                    updateFields={this.onkeyup}
                    parentIndex={i}
                    parentName="achivments"
                  />
                ))}
              </div>
              <div>
                <h3 className="TitlesForCvExp">Interests</h3>
                <div>
                  <WebInterests
                    toggle={this.props.toggle}
                    interests={this.state.cv.interests}
                    updateFields={this.onkeyup}
                    parentName="interests"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default WebCV;
