import React from "react";
import "./WebSkills.css";

const WebSkills = props => {
  return (
    <div className="WebSkills">
      {props.skills.map((skill, i) => (
        <p
          key={i}
          id={i}
          suppressContentEditableWarning="true"
          contentEditable={props.toggle ? "true" : "false"}
          onBlur={e =>
            props.updateFields(
              e.target.id,
              e.target.innerText,
              props.parentName
            )
          }
        >
          {skill}
        </p>
      ))}
    </div>
  );
};

export default WebSkills;
