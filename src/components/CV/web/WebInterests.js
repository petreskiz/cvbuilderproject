import React from "react";
import "./WebInterests.css";

const WebInterests = props => {
  return (
    <div className="WebInterests">
      {props.interests.map((int, i) => (
        <p
          id="webInterests"
          key={i}
          suppressContentEditableWarning="true"
          contentEditable={props.toggle ? "true" : "false"}
          onBlur={e =>
            props.updateFields(i, e.target.innerText, props.parentName)
          }
        >
          {int}
        </p>
      ))}
    </div>
  );
};

export default WebInterests;
