import React from "react";
import "./WebEducation.css";

const WebEducation = props => {
  return (
    <div className="WebEducation">
      <div className="ActiveDivv"></div>
      <div className="Edu">
        <h3
          className="Program"
          suppressContentEditableWarning="true"
          onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
          onBlur={e =>
            props.updateFields(
              e.target.id,
              e.target.innerText,
              props.parentName,
              props.parentIndex
            )
          }
          contentEditable={props.toggle ? "true" : "false"}
          id="program"
        >
          {props.education.program}
        </h3>
        <h4
          className="Institution"
          onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
          suppressContentEditableWarning="true"
          onBlur={e =>
            props.updateFields(
              e.target.id,
              e.target.innerText,
              props.parentName,
              props.parentIndex
            )
          }
          contentEditable={props.toggle ? "true" : "false"}
          id="institution"
        >
          {props.education.institution}
        </h4>
        <div className="DatesExperienceEdu" style={{ width: "max-content" }}>
          <input
            onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
            type="month"
            min="1900-01"
            value={props.education.startDate}
            id="startDate"
            disabled={props.toggle ? "" : "disabled"}
            onChange={e =>
              props.updateFields(
                e.target.id,
                e.target.value,
                props.parentName,
                props.parentIndex
              )
            }
          />
          {props.education.isPresent ? (
            ""
          ) : (
            <input
              type="month"
              onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
              min="1900-01"
              value={props.education.endDate}
              id="endDate"
              disabled={props.toggle ? "" : "disabled"}
              onChange={e =>
                props.updateFields(
                  e.target.id,
                  e.target.value,
                  props.parentName,
                  props.parentIndex
                )
              }
            />
          )}
          <div style={{ display: "flex" }}>
            <input
              id="isPresent"
              type="checkbox"
              checked={props.education.isPresent}
              onChange={e =>
                props.updateFields(
                  e.target.id,
                  e.target.checked,
                  props.parentName,
                  props.parentIndex
                )
              }
            />
            <p style={{ margin: "0" }}>Present</p>
          </div>
        </div>
        <ul>
          {props.education.courses.map((el, i) => (
            <li
              key={i}
              onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
              className="Courses"
              suppressContentEditableWarning="true"
              onBlur={e =>
                props.updateFields(
                  e.target.id,
                  e.target.innerText,
                  props.parentName,
                  props.parentIndex,
                  i,
                  "courses"
                )
              }
              contentEditable={props.toggle ? "true" : "false"}
              id="courses"
            >
              {el}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default WebEducation;
