import React from "react";
import "./WebCvProfile.css";
import ImgUpload from "../../ImageUpload/ImageUpload";

const WebCvProfile = (props) => {
  return (
    <div className="WebCvProfile">
      <div className="Selected"></div>
      <div className="ProfileInfo">
        <h2
          onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
          suppressContentEditableWarning="true"
          className="Name"
          id="name"
          onBlur={e =>
            props.updateFields(
              e.target.id,
              e.target.innerText,
              props.parentName
            )
          }
          contentEditable={props.toggle ? "true" : "false"}
        >
          {props.profile.name}
        </h2>
        <h5
          onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
          suppressContentEditableWarning="true"
          onBlur={e =>
            props.updateFields(
              e.target.id,
              e.target.innerText,
              props.parentName
            )
          }
          className="Jobs"
          id="jobs"
          contentEditable={props.toggle ? "true" : "false"}
        >
          {props.profile.jobs}
        </h5>
        <p
          onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
          className="Paragraph"
          suppressContentEditableWarning="true"
          onBlur={e =>
            props.updateFields(
              e.target.id,
              e.target.innerText,
              props.parentName
            )
          }
          className="About"
          contentEditable={props.toggle ? "true" : "false"}
          id="paragraph"
        >
          {props.profile.paragraph}
        </p>
      </div>
      <div
        className="DivImg"
        id="img"
        onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
      >
        <ImgUpload />
      </div>
      <div className="Contact">
        <p
          suppressContentEditableWarning="true"
          onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
          id="email"
          contentEditable={props.toggle ? "true" : "false"}
          onBlur={e =>
            props.updateFields(
              e.target.id,
              e.target.innerText,
              props.parentName
            )
          }
        >
          {props.profile.email}
          <i className="fas fa-envelope"></i>
        </p>
        <p
          suppressContentEditableWarning="true"
          onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
          id="phone"
          contentEditable={props.toggle ? "true" : "false"}
          onBlur={e =>
            props.updateFields(
              e.target.id,
              e.target.innerText,
              props.parentName
            )
          }
        >
          {props.profile.phone}
          <span>
            <i className="fas fa-mobile-alt"></i>
          </span>
        </p>

        <p
          suppressContentEditableWarning="true"
          onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
          onBlur={e =>
            props.updateFields(
              e.target.id,
              e.target.innerText,
              props.parentName
            )
          }
          id="city"
          contentEditable={props.toggle ? "true" : "false"}
        >
          {props.profile.city}
          <i className="fas fa-map-marker-alt"></i>
        </p>
        <p
          onBlur={e =>
            props.updateFields(
              e.target.id,
              e.target.innerText,
              props.parentName
            )
          }
          suppressContentEditableWarning="true"
          onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
          id="twitter"
          contentEditable={props.toggle ? "true" : "false"}
        >
          {props.profile.twitter}
          <i className="fab fa-twitter"></i>
        </p>
      </div>
    </div>
  );
};

export default WebCvProfile;
