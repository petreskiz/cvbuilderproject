import React from "react";
import "./WebCvWorkExp.css";

const WebCvWorkExp = (props) => {
  return (
    <div className="WebCvWorkExp">
      <div className="ActiveDiv"></div>
      <div className="ExDetails">
        <h4
          onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
          className="Position"
          suppressContentEditableWarning="true"
          onBlur={e =>
            props.updateFields(
              e.target.id,
              e.target.innerText,
              props.parentName,
              props.parentIndex
            )
          }
          contentEditable={props.toggle ? "true" : "false"}
          id="position"
        >
          {props.experience.position}
        </h4>
        <p
          className="Company"
          onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
          suppressContentEditableWarning="true"
          id="company"
          contentEditable={props.toggle ? "true" : "false"}
          onBlur={e =>
            props.updateFields(
              e.target.id,
              e.target.innerText,
              props.parentName,
              props.parentIndex
            )
          }
          // onMouseOver={e => props.tip2(e.target.id)}
        >
          {props.experience.company}
        </p>
        <div className="DatesExperience">
          <div>
            <input
              className="StartDate"
              onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
              type="month"
              min="1900-01"
              value={props.experience.startDate}
              id="startDate"
              disabled={props.toggle ? "" : "disabled"}
              onChange={e =>
                props.updateFields(
                  e.target.id,
                  e.target.value,
                  props.parentName,
                  props.parentIndex
                )
              }
            />
            {props.experience.isPresent ? (
              ""
            ) : (
              <input
                onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
                className="EndDate"
                type="month"
                min="1900-01"
                value={props.experience.endDate}
                id="endDate"
                disabled={props.toggle ? "" : "disabled"}
                onChange={e =>
                  props.updateFields(
                    e.target.id,
                    e.target.value,
                    props.parentName,
                    props.parentIndex
                  )
                }
              />
            )}
            <div style={{ display: "flex" }}>
              <input
                id="isPresent"
                type="checkbox"
                checked={props.experience.isPresent}
                onChange={e =>
                  props.updateFields(
                    e.target.id,
                    e.target.checked,
                    props.parentName,
                    props.parentIndex
                  )
                }
              />
              <p className="Present">Present</p>
            </div>
          </div>
          <div>
            <p
              onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
              className="CityOfWork"
              suppressContentEditableWarning="true"
              id="cityOfWork"
              contentEditable={props.toggle ? "true" : "false"}
              onBlur={e =>
                props.updateFields(
                  e.target.id,
                  e.target.innerText,
                  props.parentName,
                  props.parentIndex
                )
              }
            >
              {props.experience.cityOfWork}
            </p>
          </div>
        </div>
        <div>
          <p style={{ color: "turquoise,", margin: "0" }}>
            Accomplishments/Tasks
          </p>
          <ul className="AccomplishmentsUl">
            {props.experience.accomplishments.map((ac, i) => (
              <li
                onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
                key={i}
                onBlur={e =>
                  props.updateFields(
                    e.target.id,
                    e.target.innerText,
                    props.parentName,
                    props.parentIndex,
                    i,
                    "accomplishments"
                  )
                }
                id={`${props.experience.accomplishments[i]}`}
                suppressContentEditableWarning="true"
                contentEditable={props.toggle ? "true" : "false"}
              >
                {ac}
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default WebCvWorkExp;
