import React from "react";
import "./DataCvEducation.css";

const DataCvEducation = props => {
  return (
    <div className="DataCvEducation">
      <h3
        suppressContentEditableWarning="true"
        onBlur={e =>
          props.updateFields(
            e.target.id,
            e.target.innerText,
            props.parentName,
            props.parentIndex
          )
        }
        contentEditable={props.toggle ? "true" : "false"}
        id="program"
      >
        {props.educations.program}
      </h3>
      <h5
        suppressContentEditableWarning="true"
        onBlur={e =>
          props.updateFields(
            e.target.id,
            e.target.innerText,
            props.parentName,
            props.parentIndex
          )
        }
        contentEditable={props.toggle ? "true" : "false"}
        id="institution"
      >
        {props.educations.institution}
      </h5>
      <div className="Datess"></div>
      <p>Courses</p>
      <ul className="DataUl">
        {props.educations.courses.map((el, i) => (
          <li
            key={i}
            className=""
            suppressContentEditableWarning="true"
            onBlur={e =>
              props.updateFields(
                e.target.id,
                e.target.innerText,
                props.parentName,
                props.parentIndex,
                i,
                "courses"
              )
            }
            contentEditable={props.toggle ? "true" : "false"}
            id="courses"
          >
            {el}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default DataCvEducation;
