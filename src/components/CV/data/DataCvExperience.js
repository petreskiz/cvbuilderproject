import React from "react";
import "./DataCvExperience.css";

const DataCvExperience = (props) => {
  return (
    <div className="DataCvExperience">
      <div className="DataExperience">
        <h3
          suppressContentEditableWarning="true"
          onBlur={e =>
            props.updateFields(
              e.target.id,
              e.target.innerText,
              props.parentName,
              props.parentIndex
            )
          }
          contentEditable={props.toggle ? "true" : "false"}
          id="position"
        >
          {props.experience.position}
        </h3>
        <h4
          suppressContentEditableWarning="true"
          id="company"
          contentEditable={props.toggle ? "true" : "false"}
          onBlur={e =>
            props.updateFields(
              e.target.id,
              e.target.innerText,
              props.parentName,
              props.parentIndex
            )
          }
        >
          {props.experience.company}
        </h4>
        <div className="DataDates"></div>
        <p>Achievements/Tasks</p>
        <ul className="DataUl">
          {props.experience.accomplishments.map((ac, i) => (
            <li
              key={i}
              onBlur={e =>
                props.updateFields(
                  e.target.id,
                  e.target.innerText,
                  props.parentName,
                  props.parentIndex,
                  i,
                  "accomplishments"
                )
              }
              id={`${props.experience.accomplishments[i]}`}
              suppressContentEditableWarning="true"
              contentEditable={props.toggle ? "true" : "false"}
            >
              {ac}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default DataCvExperience;
