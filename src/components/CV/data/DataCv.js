import React, { Component } from "react";
import "./DataCv.css";
import DataCvProfile from "./DataCvProfile";
import DataCvContact from "./DataCvContact";
import DataCvExperience from "./DataCvExperience";
import DataCvEducation from "./DataCvEducation";
import DataSkills from "./DataSkills";
import DataInformal from "./DataInformal";

class DataCV extends Component {
  constructor(props) {
    super(props);
    this.state = {

      cv: {
        profile: {
          name: "Aleksandra Janakievska",
          jobs: "Data Scientist",
          paragraph:
            " Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil rem itaque enim, dolorum corporis explicabo hic velit quas vel exercitationem ex temporibus atque aliquid fuga optio! Harum error velit illum?"
        },
        contact: {
          email: "aleksandraxx@wearelaika.com",
          phone: "000-000-000"
        },
        certificates: ["Data Analysis", "Machine Learning"],
        experience: [
          {
            isPresent: false,
            position: "Data Scientist",
            company: "Rocket Corp",
            cityOfWork: "Los Angeles,Usa",
            startDate: "2002-06",
            endDate: "2003-06",
            accomplishments: [
              "Collecting,analyzing and interpreting raw data from various websites.or sit, amet consectetur adipisicing elit. Facere quis in odio repudiandae, id maiores vel iste cumque voluptas .",
              "repudiandae, id maiores vel iste cumque volupta",
              "repudiandae, id maiores vel iste cumque volupta"
            ]
          }
        ],
        languages: [
          {
            language: "English",
            level: "Full Professional Proficiency"
          }
        ],
        educations: [
          {
            isPresent: false,
            institution: "Brainster",
            program: "Data Science Academy",
            startDate: "2018-10",
            endDate: "2019-10",
            courses: ["Lead Generation"]
          },
          {
            isPresent: false,
            institution:
              "Faculty of Economy, University : 'St. Cyril and Methodius'",
            program: "Financial Managment",
            startDate: "2018-10",
            endDate: "2019-10",
            courses: ["Lead Generation"]
          }
        ],
        skills: ["Python", "MySQL", "PHP", "R", "C"],
        interests: ["Physics", "Video Games", "AI", "Reading"],
        informal: [
          "Artificial Intelligence Webinar",
          "Business Analytics Course"
        ]
      }
    };
  }

  onkeyup = (
    targetId,
    targetValue,
    parentName,
    parentIndex,
    index,
    arrayName
  ) => {
    if (index != undefined) {
      const { cv } = this.state;
      cv[parentName][parentIndex][arrayName][index] = targetValue;
      this.setState({
        cv
      });
    } else if (parentIndex != undefined) {
      const { cv } = this.state;
      cv[parentName][parentIndex][targetId] = targetValue;
      this.setState({
        cv
      });
    } else {
      const { cv } = this.state;
      cv[parentName][targetId] = targetValue;
      this.setState({
        cv
      });
    }
  };


  render() {
    return (
      <div className="CV-web">
        <DataCvProfile
          toggle={this.props.toggle}
          profile={this.state.cv.profile}
          updateFields={this.onkeyup}
          parentName="profile"
        />
        <DataCvContact
          toggle={this.props.toggle}
          contact={this.state.cv.contact}
          updateFields={this.onkeyup}
          parentName="contact"
        />
        <div className="DataExperienceandEducation">
          <div style={{ width: "48%" }}>
            <h1 className="DataWork">Work Experience</h1>
            {this.state.cv.experience.map((exp, i) => (
              <DataCvExperience
                key={i}
                toggle={this.props.toggle}
                experience={exp}
                updateFields={this.onkeyup}
                parentIndex={i}
                parentName="experience"
              />
            ))}
            <h1 className="DataWork">Education</h1>
            {this.state.cv.educations.map((edu, i) => (
              <DataCvEducation
                key={i}
                toggle={this.props.toggle}
                educations={edu}
                updateFields={this.onkeyup}
                parentIndex={i}
                parentName="educations"
              />
            ))}
          </div>
          <div style={{ width: "48%" }}>
            <h1 className="DataWork">SKILLS</h1>
            <DataSkills
              skills={this.state.cv.skills}
              toggle={this.props.toggle}
              updateFields={this.onkeyup}
              parentName="skills"
            />
            <h1 className="DataWork">Certificates</h1>
            <div>
              {this.state.cv.certificates.map((ser, i) => (
                <h4
                key={i}
                  suppressContentEditableWarning="true"
                  contentEditable={this.props.toggle ? "true" : "false"}
                  onBlur={e =>
                    this.onkeyup(i, e.target.innerText, "certificates")
                  }
                  className="DataCertificates"
                >
                  {ser}
                </h4>
              ))}
            </div>
            <h1 className="DataWork">Languages</h1>
            <div className="DataLanguages">
              {this.state.cv.languages.map((lang, i) => (
                <div key={i}>
                  <h4
                    suppressContentEditableWarning="true"
                    contentEditable={this.props.toggle ? "true" : "false"}
                    onBlur={e =>
                      this.onkeyup(
                        e.target.id,
                        e.target.innerText,
                        "languages",
                        i
                      )
                    }
                    id="language"
                  >
                    {lang.language}
                  </h4>
                  <p
                    suppressContentEditableWarning="true"
                    contentEditable={this.props.toggle ? "true" : "false"}
                    onBlur={e =>
                      this.onkeyup(
                        e.target.id,
                        e.target.innerText,
                        "languages",
                        i 
                      )
                    }
                    id="level"
                  >
                    {lang.level}
                  </p>
                </div>
              ))}
            </div>
            <div>
              <h1 className="DataWork">Informal Education</h1>
                <DataInformal
                toggle={this.props.toggle}
                informal={this.state.cv.informal}
                updateFields={this.onkeyup}
                parentName="informal"
                />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DataCV;
