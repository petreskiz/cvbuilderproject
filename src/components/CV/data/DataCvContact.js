import React from "react";
import "./DataCvContact.css";

const DataCvContact = props => {
  return (
    <div className="MarketingCvContact">
      <i className="fas fa-envelope"></i>
      <p
        suppressContentEditableWarning="true"
        // onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
        id="email"
        contentEditable={props.toggle ? "true" : "false"}
        onBlur={e =>
          props.updateFields(e.target.id, e.target.innerText, props.parentName)
        }
      >
        {props.contact.email}
      </p>
      <i className="fas fa-mobile-alt"></i>
      <p
        suppressContentEditableWarning="true"
        id="phone"
        contentEditable={props.toggle ? "true" : "false"}
        onBlur={e =>
          props.updateFields(e.target.id, e.target.innerText, props.parentName)
        }
      >
        {props.contact.phone}
      </p>
    </div>
  );
};

export default DataCvContact;
