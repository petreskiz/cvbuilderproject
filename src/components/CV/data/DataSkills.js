import React from "react";
import "./DataSkills.css";

const DataSkills = props => {
  return (
    <div className="DataSkills">
      <ul className="DataSkillsUl">
        {props.skills.map((skill, i) => (
          <li
          key={i}
            suppressContentEditableWarning="true"
            contentEditable={props.toggle ? "true" : "false"}
            onBlur={e =>
              props.updateFields(i, e.target.innerText, props.parentName)
            }
          >
            {skill}
            <div>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default DataSkills;
