import React from "react";
import "./DataInformal.css";

const DataInformal = props => {
  return (
    <div className="DataInformal">
      {props.informal.map((el, i) => (
        <p
          id={i}
          key={i}
          suppressContentEditableWarning="true"
          onBlur={e =>
            props.updateFields(
              e.target.id,
              e.target.innerText,
              props.parentName,
              props.parentIndex
            )
          }
          contentEditable={props.toggle ? "true" : "false"}
        >
          {el}
        </p>
      ))}
    </div>
  );
};

export default DataInformal;
