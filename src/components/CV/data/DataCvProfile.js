import React from "react";
import "./DataCvProfile.css";
import ImageUpload from '../../ImageUpload/ImageUpload'

const DataCvProfile = props => {
  return (
    <div className="DataCvProfile">
      <div className="DataImg">
        <ImageUpload/>
      </div>
      <div className="DataProfileInfo">
        <h1
          suppressContentEditableWarning="true"
          // onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
          id="name"
          contentEditable={props.toggle ? "true" : "false"}
          onBlur={e =>
            props.updateFields(
              e.target.id,
              e.target.innerText,
              props.parentName
            )
          }
          className="DataProfileName"
        >
          {props.profile.name}
        </h1>
        <h4
          suppressContentEditableWarning="true"
          // onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
          id="jobs"
          contentEditable={props.toggle ? "true" : "false"}
          onBlur={e =>
            props.updateFields(
              e.target.id,
              e.target.innerText,
              props.parentName
            )
          }
          className="DataProfileProffesion"
        >
          {props.profile.jobs}
        </h4>
        <p
          suppressContentEditableWarning="true"
          // onMouseOver={e => props.tip2(`webcv${e.target.id}`)}
          id="paragraph"
          contentEditable={props.toggle ? "true" : "false"}
          onBlur={e =>
            props.updateFields(
              e.target.id,
              e.target.innerText,
              props.parentName
            )
          }
          className="DataProfileAbout"
        >
          {props.profile.paragraph}
        </p>
      </div>
    </div>
  );
};

export default DataCvProfile;
