import React, { Component } from "react";
import WebCv from "./web/WebCv";
import DataCv from "./data/DataCv";
import MarketingCV from "./marketing/MarketingCv";
import DesignCV from "./design/DesignCv";
import "./CV.css";
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';

class CV extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: false
    };
  }
  onclick = e => {
    this.setState({
      toggle: !this.state.toggle
    });
  };

  download = () => {
    var HTML_Width = document.getElementsByClassName("cv-content")[0].clientWidth;
    var HTML_Height = document.getElementsByClassName("cv-content")[0].clientHeight;
    var top_left_margin = 15;
    var PDF_Width = HTML_Width + top_left_margin * 2;
    var PDF_Height = HTML_Height*1.5 + top_left_margin * 2;
    var canvas_image_width = HTML_Width;
    var canvas_image_height = HTML_Height;

    var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;

    window.scrollTo(0,0);  

    html2canvas(document.getElementsByClassName("cv-content")[0], {
      allowTaint: true
    }).then(function(canvas) {
      canvas.getContext("2d");

      console.log(canvas.height + "  " + canvas.width);

      var imgData = canvas.toDataURL("image/jpeg", 1.0);
      var pdf = new jsPDF("p", "pt", [PDF_Width, PDF_Height]);
      pdf.addImage(
        imgData,
        "JPG",
        top_left_margin,
        top_left_margin,
        canvas_image_width,
        canvas_image_height
      );

      for (var i = 1; i <= totalPDFPages; i++) {
        pdf.addPage(PDF_Width, PDF_Height);
        pdf.addImage(
          imgData,
          "JPG",
          top_left_margin,
          -(PDF_Height * i) + top_left_margin * 4,
          canvas_image_width,
          canvas_image_height
        );
      }

      pdf.save("cv.pdf");
    });
  };
  render() {
    return (
      <div className="CV" >
        <button
        className='buttons'
          onClick={this.onclick}
          style={{
            position: "absolute",
            right: "0",
            top: "0",
            width: "max-content"
          }}
        >
          {!this.state.toggle ? "Edit" : "Save"}
        </button>
        <div className="cv-content" >
          {this.props.category === "web" && (
            <WebCv toggle={this.state.toggle} tip1={this.props.tip} />
          )}
          {this.props.category === "data" && (
            <DataCv toggle={this.state.toggle} tip1={this.props.tip} />
          )}
          {this.props.category === "marketing" && (
            <MarketingCV toggle={this.state.toggle} tip1={this.props.tip} />
          )}
          {this.props.category === "design" && (
            <div> <DesignCV toggle={this.state.toggle} tip1={this.props.tip} /> </div>
          )}
        </div>
        <button
        className='buttons'
         style={{
          position: "absolute",
          right: "0",
          bottom: "0",
          width: "max-content"
        }} onClick={() => this.download()}>Download</button>
      </div>
    );
  }
}

export default CV;
