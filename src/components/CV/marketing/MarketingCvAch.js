import React, { Component } from "react";
import "./MarketingCvAch.css";

class MarketingCvAch extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="MarketingCvAch">
        <h3
          className="MarketingTitle"
          suppressContentEditableWarning="true"
          onBlur={e =>
            this.props.updateFields(
              e.target.id,
              e.target.innerText,
              this.props.parentName,
              this.props.parentIndex
            )
          }
          contentEditable={this.props.toggle ? "true" : "false"}
          id="title"
        >
          {this.props.achivments.title}
        </h3>
        <p
          className="MarketingDescription"
          suppressContentEditableWarning="true"
          onBlur={e =>
            this.props.updateFields(
              e.target.id,
              e.target.innerText,
              this.props.parentName,
              this.props.parentIndex
            )
          }
          contentEditable={this.props.toggle ? "true" : "false"}
          id="description"
        >
          {this.props.achivments.description}
        </p>
      </div>
    );
  }
}

export default MarketingCvAch;
