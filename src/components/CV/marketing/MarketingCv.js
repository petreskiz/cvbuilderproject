import React, { Component } from "react";
import "./MarketingCv.css";
import MarketingCvProfile from "./MarketingCvProfile";
import MarketingCvContact from "./MarketingCvContact";
import MarketingCvExperience from "./MarketingCvExperience";
import MarketingCvEducation from "./MarketingCvEducation";
import MarketingSkills from "./MarketingSkill";
import MarketingCvAch from "./MarketingCvAch";
import MarketingCvLang from './MarketingCvLang'

class MarketingCV extends Component {
  constructor(props) {
    super(props);
    this.state = {
      
      cv: {
        profile: {
          name: "Stefanija Tenekedjieva",
          jobs: "Digital Marketer",
          paragraph:
            " Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil rem itaque enim, dolorum corporis explicabo hic velit quas vel exercitationem ex temporibus atque aliquid fuga optio! Harum error velit illum?"
        },
        contact: {
          email: "stefanijatenekedjieva@gmail.com",
          phone: "phone number"
        },
        certificates: ["Data Analysis", "Machine Learning"],
        experience: [
          {
            isPresent: false,
            position: "Digital Marketer",
            company: "Laika",
            cityOfWork: "Los Angeles,Usa",
            startDate: "2002-06",
            endDate: "2003-06",
            accomplishments: [
              "Content writing",
              "social media advertising",
              "lead generation",
              "conversion rate optimisation"
            ]
          },
          {
            isPresent: false,
            position: "Project Coordinator",
            company: "Radio MOF",
            cityOfWork: "Los Angeles,Usa",
            startDate: "2002-06",
            endDate: "2003-06",
            accomplishments: [
              "Content writing",
              "social media advertising",
              "lead generation",
              "conversion rate optimisation"
            ]
          },
          {
            isPresent: false,
            position: "Journalist",
            company: "Radio MOF",
            cityOfWork: "Los Angeles,Usa",
            startDate: "2002-06",
            endDate: "2003-06",
            accomplishments: [
              "Content writing",
              "social media advertising",
              "lead generation",
              "conversion rate optimisation"
            ]
          }
        ], achivments: [
            {
              title: "IEEE Honorary Membership (2015)",
              description:
                "Given to people who have rendered maritorius service to humanity"
            },
            {
              title: "53rd Richest person in the world - Forbes (2018)"
            }
          ],
        languages: [
          {
            language: "English",
            level: "Full Professional Proficiency"
          }
        ],
        educations: [
          {
            isPresent: false,
            institution: "Brainster Digital Marketing",
            program: "Education",
            startDate: "2018-10",
            endDate: "2019-10",
            courses: [
              "Integrated Marketing Communication",
              "Sales",
              "Lead Generation"
            ]
          }
        ],
        skills: [
          "Google Ads",
          "Facebook Ads Manager",
          "Google Analitics",
          "Google Data Studio",
          "WordPress",
          "Canva",
          "Adobe Premiere",
          "Content Writer"
        ],
        interests: ["Physics", "Video Games", "AI", "Reading"],
        informal: [
          "Artificial Intelligence Webinar",
          "Business Analytics Course"
        ]
      }
    };
  }

  onkeyup = (
    targetId,
    targetValue,
    parentName,
    parentIndex,
    index,
    arrayName
  ) => {
    if (index != undefined) {
      const { cv } = this.state;
      cv[parentName][parentIndex][arrayName][index] = targetValue;
      this.setState({
        cv
      });
    } else if (parentIndex != undefined) {
      const { cv } = this.state;
      cv[parentName][parentIndex][targetId] = targetValue;
      this.setState({
        cv
      });
    } else {
      const { cv } = this.state;
      cv[parentName][targetId] = targetValue;
      this.setState({
        cv
      });
    }
  };
 
  render() {
    return (
      <div className="CV-marketing">
        <MarketingCvProfile
          toggle={this.props.toggle}
          profile={this.state.cv.profile}
          updateFields={this.onkeyup}
          parentName="profile"
        />
        <MarketingCvContact
          toggle={this.props.toggle}
          contact={this.state.cv.contact}
          updateFields={this.onkeyup}
          parentName="contact"
        />
        <div className="MarketingExperienceandEducation">
          <div style={{ width: "48%" }}>
            <h1 className="MarketingWork">Work Experience</h1>
            {this.state.cv.experience.map((exp, i) => (
              <MarketingCvExperience
                key={i}
                toggle={this.props.toggle}
                experience={exp}
                updateFields={this.onkeyup}
                parentIndex={i}
                parentName="experience"
              />
            ))}
            <h1 className="MarketingWork">Education</h1>
            {this.state.cv.educations.map((edu, i) => (
              <MarketingCvEducation
                key={i}
                toggle={this.props.toggle}
                educations={edu}
                updateFields={this.onkeyup}
                parentIndex={i}
                parentName="educations"
              />
            ))}
          </div>
          <div style={{ width: "48%" }}>
            <h1 className="MarketingWork">SKILLS</h1>
            <MarketingSkills
              skills={this.state.cv.skills}
              toggle={this.props.toggle}
              updateFields={this.onkeyup}
              parentName="skills"
            />
            <h1 className="MarketingWork">Achievements</h1>
            {this.state.cv.achivments.map((ach, i) => (
              <MarketingCvAch
                key={i}
                toggle={this.props.toggle}
                achivments={ach}
                updateFields={this.onkeyup}
                parentIndex={i}
                parentName="achivments"
              />
            ))}
            <h1 className="MarketingWork">Languages</h1>
            {this.state.cv.languages.map((ach, i) => (
              <MarketingCvLang
                key={i}
                toggle={this.props.toggle}
                language={ach}
                updateFields={this.onkeyup}
                parentIndex={i}
                parentName="languages"
              />
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default MarketingCV;
