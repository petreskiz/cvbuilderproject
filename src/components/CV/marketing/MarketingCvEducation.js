import React, { Component } from "react";
import "./MarketingCvEducation.css";

class MarketingCvEducation extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="MarketingCvEducation">
        <h3
          suppressContentEditableWarning="true"
          onBlur={e =>
            this.props.updateFields(
              e.target.id,
              e.target.innerText,
              this.props.parentName,
              this.props.parentIndex
            )
          }
          contentEditable={this.props.toggle ? "true" : "false"}
          id="program"
        >
          {this.props.educations.program}
        </h3>
        <h5
          suppressContentEditableWarning="true"
          onBlur={e =>
            this.props.updateFields(
              e.target.id,
              e.target.innerText,
              this.props.parentName,
              this.props.parentIndex
            )
          }
          contentEditable={this.props.toggle ? "true" : "false"}
          id="institution"
        >
          {this.props.educations.institution}
        </h5>
        <div className="MarketingDatess"></div>
        <p>Courses</p>
        <ul className="MarketingUl">
          {this.props.educations.courses.map((el, i) => (
            <li
              key={i}
              className=""
              suppressContentEditableWarning="true"
              onBlur={e =>
                this.props.updateFields(
                  e.target.id,
                  e.target.innerText,
                  this.props.parentName,
                  this.props.parentIndex,
                  i,
                  "courses"
                )
              }
              contentEditable={this.props.toggle ? "true" : "false"}
              id="courses"
            >
              <span> {el}</span>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default MarketingCvEducation;
