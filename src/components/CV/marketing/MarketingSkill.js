import React, { Component } from "react";
import "./MarketingSkills.css";

class MarketingSkills extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="MarketingSkills">
        {this.props.skills.map((skill, i) => (
          <p
            key={i}
            id={i}
            suppressContentEditableWarning="true"
            contentEditable={this.props.toggle ? "true" : "false"}
            onBlur={e =>
              this.props.updateFields(
                e.target.id,
                e.target.innerText,
                this.props.parentName
              )
            }
          >
            {skill}
          </p>
        ))}
      </div>
    );
  }
}

export default MarketingSkills;
