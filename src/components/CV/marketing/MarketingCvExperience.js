import React, { Component } from "react";
import "./MarketingCvExperience.css";

class MarketingCvExperience extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="MarketingCvExperience">
        <div className="MarketingExperience">
          <h3
            suppressContentEditableWarning="true"
            onBlur={e =>
              this.props.updateFields(
                e.target.id,
                e.target.innerText,
                this.props.parentName,
                this.props.parentIndex
              )
            }
            contentEditable={this.props.toggle ? "true" : "false"}
            id="position"
          >
            {this.props.experience.position}
          </h3>
          <h4
            suppressContentEditableWarning="true"
            id="company"
            contentEditable={this.props.toggle ? "true" : "false"}
            onBlur={e =>
              this.props.updateFields(
                e.target.id,
                e.target.innerText,
                this.props.parentName,
                this.props.parentIndex
              )
            }
          >
            {this.props.experience.company}
          </h4>
          <div className="MarketingDates"></div>
          <p>Achievements/Tasks</p>
          <ul className="MarketingExUl">
            {this.props.experience.accomplishments.map((ac, i) => (
              <li
                key={i}
                onBlur={e =>
                  this.props.updateFields(
                    e.target.id,
                    e.target.innerText,
                    this.props.parentName,
                    this.props.parentIndex,
                    i,
                    "accomplishments"
                  )
                }
                id={`${this.props.experience.accomplishments[i]}`}
                suppressContentEditableWarning="true"
                contentEditable={this.props.toggle ? "true" : "false"}
              >
                <span>{ac}</span>
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default MarketingCvExperience;
