import React, { Component } from "react";
import "./MarketingCvContact.css";

class MarketingCvContact extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="MarketingCvContact">
        <i className="fas fa-envelope"></i>
        <p
          suppressContentEditableWarning="true"
          // onMouseOver={e => this.props.tip2(`webcv${e.target.id}`)}
          id="email"
          contentEditable={this.props.toggle ? "true" : "false"}
          onBlur={e =>
            this.props.updateFields(
              e.target.id,
              e.target.innerText,
              this.props.parentName
            )
          }
        >
          {this.props.contact.email}
        </p>
        <i className="fas fa-mobile-alt"></i>
        <p
          suppressContentEditableWarning="true"
          // onMouseOver={e => this.props.tip2(`webcv${e.target.id}`)}
          id="phone"
          contentEditable={this.props.toggle ? "true" : "false"}
          onBlur={e =>
            this.props.updateFields(
              e.target.id,
              e.target.innerText,
              this.props.parentName
            )
          }
        >
          {this.props.contact.phone}
        </p>
      </div>
    );
  }
}

export default MarketingCvContact;
