import React, { Component } from "react";
import "./MarketingCvProfile.css";
import ImageUpload from '../../ImageUpload/ImageUpload'

class MarketingCvProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="MarketingCvProfile">
        <div className="MarketingImg">
          <ImageUpload/>
        </div>
        <div className="MarketingProfileInfo">
          <h1
            suppressContentEditableWarning="true"
            // onMouseOver={e => this.props.tip2(`webcv${e.target.id}`)}
            id="name"
            contentEditable={this.props.toggle ? "true" : "false"}
            onBlur={e =>
              this.props.updateFields(
                e.target.id,
                e.target.innerText,
                this.props.parentName
              )
            }
            className="MarketingProfileName"
          >
            {this.props.profile.name}
          </h1>
          <h4
            suppressContentEditableWarning="true"
            // onMouseOver={e => this.props.tip2(`webcv${e.target.id}`)}
            id="jobs"
            contentEditable={this.props.toggle ? "true" : "false"}
            onBlur={e =>
              this.props.updateFields(
                e.target.id,
                e.target.innerText,
                this.props.parentName
              )
            }
            className="MarketingProfileProffesion"
          >
            {this.props.profile.jobs}
          </h4>
          <p
            suppressContentEditableWarning="true"
            // onMouseOver={e => this.props.tip2(`webcv${e.target.id}`)}
            id="paragraph"
            contentEditable={this.props.toggle ? "true" : "false"}
            onBlur={e =>
              this.props.updateFields(
                e.target.id,
                e.target.innerText,
                this.props.parentName
              )
            }
            className="MarketingProfileAbout"
          >
            {this.props.profile.paragraph}
          </p>
        </div>
      </div>
    );
  }
}

export default MarketingCvProfile;
