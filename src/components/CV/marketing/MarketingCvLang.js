import React, { Component } from "react";
import "./MarketingCvLang.css";

class MarketingCvLang extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="MarketingCvLang">
        <h3
          className="MarketingLang"
          suppressContentEditableWarning="true"
          onBlur={e =>
            this.props.updateFields(
              e.target.id,
              e.target.innerText,
              this.props.parentName,
              this.props.parentIndex
            )
          }
          contentEditable={this.props.toggle ? "true" : "false"}
          id="language"
        >
          {this.props.language.language}
        </h3>
        <p
          className="MarketingLevel"
          suppressContentEditableWarning="true"
          onBlur={e =>
            this.props.updateFields(
              e.target.id,
              e.target.innerText,
              this.props.parentName,
              this.props.parentIndex
            )
          }
          contentEditable={this.props.toggle ? "true" : "false"}
          id="level"
        >
          {this.props.language.level}
        </p>
      </div>
    );
  }
}

export default MarketingCvLang;
