import React, { Component } from "react";
import "./DesignSkills.css";

class DesignSkills extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="DesignSkills">
        {this.props.skills.map((skill, i) => (
          <p
          key={i}
            suppressContentEditableWarning="true"
            contentEditable={this.props.toggle ? "true" : "false"}
            onBlur={e =>
              this.props.updateFields(
                i,
                e.target.innerText,
                this.props.parentName
              )
            }
          >
            {skill}
          </p>
        ))}
      </div>
    );
  }
}

export default DesignSkills;
