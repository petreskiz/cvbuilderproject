import React, { Component } from "react";
import "./DesignProfile.css";

class DesignProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="DesignProfile">
        <h2
          suppressContentEditableWarning="true"
          // onMouseOver={e => this.props.tip2(`webcv${e.target.id}`)}
          id="name"
          contentEditable={this.props.toggle ? "true" : "false"}
          onBlur={e =>
            this.props.updateFields(
              e.target.id,
              e.target.innerText,
              this.props.parentName
            )
          }
        >
          {this.props.profile.name}
        </h2>
        <p
          suppressContentEditableWarning="true"
          // onMouseOver={e => this.props.tip2(`webcv${e.target.id}`)}
          id="jobs"
          contentEditable={this.props.toggle ? "true" : "false"}
          onBlur={e =>
            this.props.updateFields(
              e.target.id,
              e.target.innerText,
              this.props.parentName
            )
          }
        >
          {this.props.profile.jobs}
        </p>
      </div>
    );
  }
}

export default DesignProfile;
