import React, { Component } from "react";
import "./DesignContact.css";

class DesignContact extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="DesignContact">
        <p
          suppressContentEditableWarning="true"
          // onMouseOver={e => this.props.tip2(`webcv${e.target.id}`)}
          id="phone"
          contentEditable={this.props.toggle ? "true" : "false"}
          onBlur={e =>
            this.props.updateFields(
              e.target.id,
              e.target.innerText,
              this.props.parentName
            )
          }
        >
          {this.props.contact.phone}
        </p>
        <p
          suppressContentEditableWarning="true"
          // onMouseOver={e => this.props.tip2(`webcv${e.target.id}`)}
          id="email"
          contentEditable={this.props.toggle ? "true" : "false"}
          onBlur={e =>
            this.props.updateFields(
              e.target.id,
              e.target.innerText,
              this.props.parentName
            )
          }
        >
          {this.props.contact.email}
        </p>
        <p
          suppressContentEditableWarning="true"
          // onMouseOver={e => this.props.tip2(`webcv${e.target.id}`)}
          id="linkedin"
          contentEditable={this.props.toggle ? "true" : "false"}
          onBlur={e =>
            this.props.updateFields(
              e.target.id,
              e.target.innerText,
              this.props.parentName
            )
          }
        >
          {this.props.contact.linkedin}
        </p>
        <p
          suppressContentEditableWarning="true"
          // onMouseOver={e => this.props.tip2(`webcv${e.target.id}`)}
          id="city"
          contentEditable={this.props.toggle ? "true" : "false"}
          onBlur={e =>
            this.props.updateFields(
              e.target.id,
              e.target.innerText,
              this.props.parentName
            )
          }
        >
          {this.props.contact.city}
        </p>
      </div>
    );
  }
}

export default DesignContact;
