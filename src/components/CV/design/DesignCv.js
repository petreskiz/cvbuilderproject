import React, { Component } from "react";
import "./DesignCv.css";
import DesignProfile from "./DesignProfile";
import DesignAbouth from "./DesignAbouth";
import DesignExperience from "./DesignExperience";
import DesignContact from "./DesignContact";
import DesignEducation from "./DesignEducation";
import DesignSkills from "./DesignSkills";

class DesignCV extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cv: {
        profile: {
          name: "Kiril Nikolovski",
          jobs: "Graphic Designer"
        },
        abouth: {
          abouth:
            " Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatvelit cum repellendus laborum, eligendi natus molestias modi doloresaliquam sunt numquam rerum architecto culpa maiores ipsa corporisaccusantium aperiam? Deserunt."
        },
        experience: [
          {
            isPresent: false,
            position: "Junior Graphic Designer",
            company: " Brainster",
            cityOfWork: "Los Angeles,Usa",
            startDate: "2002-06",
            endDate: "2003-06",
            accomplishments: [
              "Collecting,analyzing and interpreting raw data from various websites.or sit, amet consectetur adipisicing elit. Facere quis in odio repudiandae, id maiores vel iste cumque voluptas .",
              "repudiandae, id maiores vel iste cumque volupta",
              "repudiandae, id maiores vel iste cumque volupta"
            ]
          }
        ],
        contact: {
          email: "kirilnikolovski@gmail.com",
          city: "Skopje,Macedonia",
          phone: "000-000-000",
          linkedin: "linked.com/in/kiril-nikoloski"
        },
        educations: [
          {
            isPresent: false,
            institution: "Brainster Academy of Design",
            program: "Data Science Academy",
            startDate: "2018-10",
            endDate: "2019-10",
            courses: ["Industrial Design"]
          }
        ],
        skills: [
          "Adobe Photoshop",
          "Adobe Ilustrator",
          "Adobe inDesign",
          "Adobe Premiere Pro",
          "MS Office"
        ]
      }
    };
  }

  onkeyup = (
    targetId,
    targetValue,
    parentName,
    parentIndex,
    index,
    arrayName
  ) => {
    if (index != undefined) {
      const { cv } = this.state;
      cv[parentName][parentIndex][arrayName][index] = targetValue;
      this.setState({
        cv
      });
    } else if (parentIndex != undefined) {
      const { cv } = this.state;
      cv[parentName][parentIndex][targetId] = targetValue;
      this.setState({
        cv
      });
    } else {
      const { cv } = this.state;
      cv[parentName][targetId] = targetValue;
      this.setState({
        cv
      });
    }
  };


  render() {
    return (
      <div className="CV-design">
        <DesignProfile
          toggle={this.props.toggle}
          profile={this.state.cv.profile}
          updateFields={this.onkeyup}
          parentName="profile"
        />
        <DesignAbouth
          toggle={this.props.toggle}
          abouth={this.state.cv.abouth}
          updateFields={this.onkeyup}
          parentName="abouth"
        />
        <div className="DesignInfo">
          <div className="DesignExperienceContact">
            <h1 className="DesignTitles">Work Experience</h1>
            {this.state.cv.experience.map((exp, i) => (
              <DesignExperience
                key={i}
                toggle={this.props.toggle}
                experience={exp}
                updateFields={this.onkeyup}
                parentIndex={i}
                parentName="experience"
              />
            ))}
            <h1 className="DesignTitles">Contact</h1>
            <DesignContact
              toggle={this.props.toggle}
              contact={this.state.cv.contact}
              updateFields={this.onkeyup}
              parentName="contact"
            />
          </div>
          <div className="DesignEducationSkills">
            <h1 className="DesignTitles">Education</h1>
            {this.state.cv.educations.map((exp, i) => (
              <DesignEducation
                key={i}
                toggle={this.props.toggle}
                educations={exp}
                updateFields={this.onkeyup}
                parentIndex={i}
                parentName="educations"
              />
            ))}

            <h1 className="DesignTitles">Skills</h1>
            <DesignSkills
              skills={this.state.cv.skills}
              toggle={this.props.toggle}
              updateFields={this.onkeyup}
              parentName="skills"
            />
          </div>
        </div>
      </div>
    );
  }
}

export default DesignCV;
