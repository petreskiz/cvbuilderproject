import React, { Component } from "react";
import "./DesignAbouth.css";

class DesignAbouth extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="DesignAbouth">
        <p 
         suppressContentEditableWarning="true"
         // onMouseOver={e => this.props.tip2(`webcv${e.target.id}`)}
         id="abouth"
         contentEditable={this.props.toggle ? "true" : "false"}
         onBlur={e =>
           this.props.updateFields(
             e.target.id,
             e.target.innerText,
             this.props.parentName
           )
         }>
         {this.props.abouth.abouth}
        </p>
      </div>
    );
  }
}

export default DesignAbouth;
