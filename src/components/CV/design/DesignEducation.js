import React, { Component } from "react";
import "./DesignEducation.css";

class DesignEducation extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="DesignEducation">
        <div className="DesignDates"></div>
        <h3
          suppressContentEditableWarning="true"
          onBlur={e =>
            this.props.updateFields(
              e.target.id,
              e.target.innerText,
              this.props.parentName,
              this.props.parentIndex
            )
          }
          contentEditable={this.props.toggle ? "true" : "false"}
          id="institution"
        >
          {this.props.educations.institution}
        </h3>
        <p
          suppressContentEditableWarning="true"
          onBlur={e =>
            this.props.updateFields(
              e.target.id,
              e.target.innerText,
              this.props.parentName,
              this.props.parentIndex
            )
          }
          contentEditable={this.props.toggle ? "true" : "false"}
          id="program"
        >
          {this.props.educations.program}
        </p>
        {this.props.educations.courses.map((ac, i) => (
          <p
       
            key={i}
            onBlur={e =>
              this.props.updateFields(
                e.target.id,
                e.target.innerText,
                this.props.parentName,
                this.props.parentIndex,
                i,
                "courses"
              )
            }
            id={`${this.props.educations.courses[i]}`}
            suppressContentEditableWarning="true"
            contentEditable={this.props.toggle ? "true" : "false"}
          >
            {ac}
          </p>
        ))}
      </div>
    );
  }
}

export default DesignEducation;
