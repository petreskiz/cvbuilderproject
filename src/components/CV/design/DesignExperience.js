import React, { Component } from "react";
import "./DesignExperience.css";

class DesignExperience extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="DesignExperience">
        <div className="DesignDates"></div>
        <h3
          suppressContentEditableWarning="true"
          id="company"
          contentEditable={this.props.toggle ? "true" : "false"}
          onBlur={e =>
            this.props.updateFields(
              e.target.id,
              e.target.innerText,
              this.props.parentName,
              this.props.parentIndex
            )
          }
        >
          {this.props.experience.company}
        </h3>
        <h4
          suppressContentEditableWarning="true"
          onBlur={e =>
            this.props.updateFields(
              e.target.id,
              e.target.innerText,
              this.props.parentName,
              this.props.parentIndex
            )
          }
          contentEditable={this.props.toggle ? "true" : "false"}
          id="position"
        >
          {this.props.experience.position}
        </h4>
        <ul>
          {this.props.experience.accomplishments.map((ac, i) => (
            <li
              key={i}
              onBlur={e =>
                this.props.updateFields(
                  e.target.id,
                  e.target.innerText,
                  this.props.parentName,
                  this.props.parentIndex,
                  i,
                  "accomplishments"
                )
              }
              id={`${this.props.experience.accomplishments[i]}`}
              suppressContentEditableWarning="true"
              contentEditable={this.props.toggle ? "true" : "false"}
            >
              {ac}
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default DesignExperience;
