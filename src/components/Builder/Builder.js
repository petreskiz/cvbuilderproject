import React, { Component } from "react";
import { Link, Switch, Route } from "react-router-dom";
import CV from "../CV/CV";
import Portfolio from "../Portfolio/Portfolio";
import LinkedIn from "../LinkedIn/LinkedIn";
import Footer from "../Footer/Footer";
import Tips from "../Tips/Tips";
import "./Builder.css";
import { tips } from "../../tips";

class Builder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category: this.props.match.params.category,
      tips: ""
    };
  }
  onTips = tip => {
    this.setState({ tips: tips[tip] });
  };
  render() {
    return (
      <div>
        <div className="Builder">
          <div className="BuilderLeft">
            <div className="BuilderAll">
              <div className="BuilderButtons">
                <Link to={`/categories/${this.state.category}/cv`}>
                  <button className="buttons">cv</button>
                </Link>
                <Link to={`/categories/${this.state.category}/linkedIn`}>
                  <button className="buttons">linkedIn</button>
                </Link>
                <Link to={`/categories/${this.state.category}/portfolio`}>
                  <button className="buttons">WEARELAIKA.COM</button>
                </Link>
              </div>

              <Switch>
                <Route
                  path={`/categories/:category/cv`}
                  render={() => (
                    <CV category={this.state.category} tip={this.onTips} />
                  )}
                />
                <Route
                  path={`/categories/:category/linkedIn`}
                  component={LinkedIn}
                />
                <Route
                  path={`/categories/:category/portfolio`}
                  component={Portfolio}
                />
              </Switch>
            </div>
          </div>
          <div className="BuilderTips">
            <Tips tip={this.state.tips} />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Builder;
