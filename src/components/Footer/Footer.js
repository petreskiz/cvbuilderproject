import React, { Component } from "react";
import "./Footer.css";

const Footer = () => {
  return (
    <div className="Footer">
      <div className="yellow">
        <p>Do you want to learn hands-on digital skills?</p>
        <button className="visit"><a className="visitA" target="_Blank" href="https://brainster.co/">Visit Brainster</a></button>
      </div>
      <div className="laika">
        <p>Do you want to recieve job offers by IT Companies?</p>
        <button className="visit"><a className="visitA" target="_Blank" href="https://www.wearelaika.com/">Visit Laika</a></button>
      </div>
    </div>
  );
};

export default Footer;
