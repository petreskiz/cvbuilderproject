import React  from "react";
import "./CategoryItem.css";
import { Link } from "react-router-dom";

const CategoryItem = props => {
  return (
    <div className="CategoryItem">
      <h3>{props.title}</h3>
      <Link to={`categories/${props.url}/cv`} >
        <button>CHOOSE</button>
      </Link>
    </div>
  );
};

export default CategoryItem;
