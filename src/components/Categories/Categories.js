import React from "react";
import "./Categories.css";
import CategoryItem from "./CategoryItem";
import Footer from "../Footer/Footer";

const Categories = props => {
  return (
    <div className="AllCategories">
      <h1>Choose your category</h1>
      <div className="Categories">
        <CategoryItem title="Web Development" url="web" />
        <CategoryItem title="Data Science" url="data" />
        <CategoryItem title="Digital Marketing" url="marketing" />
        <CategoryItem title="Design" url="design" />
      </div>
      <Footer />
    </div>
  );
};

export default Categories;
