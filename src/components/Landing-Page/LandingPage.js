import React from "react";
import "./LandingPage.css";
import { Link } from "react-router-dom";

const FirstPage = () => {
  return (
    <div>
      <div className="FirstPage">
        <div className="FirstPageText">
          <h1>The Ultimate</h1>
          <h1>CV & Portfolio Check - List</h1>
          <p>
            Are you a Web Developer, Data Scientist, Digital Marketer or a
            Designer? Have your CV and portfolioin check and create a 5-star
            representation of your skills with this guide.
          </p>
          <Link to="/categories">
            <button className="FirstPageButton">STEP INSIDE</button>
          </Link>
        </div>
        <div className="FirstPageImage">
          <img src={"/assets/img/Background_for_first_page.png"} alt="" />
        </div>
      </div>
      <div className="footer">
        <p>
          Created with 3 by the <a target="_Blank" href="http://codepreneurs.co/">Brainster Coding Academy</a> students
          and <a target="_Blank" href="https://www.wearelaika.com/">wearelaika.com</a>
        </p>
      </div>
    </div>
  );
};

export default FirstPage;
