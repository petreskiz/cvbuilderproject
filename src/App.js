import React from "react";
import "./App.css";
import LandingPage from "./components/Landing-Page/LandingPage";
import Categories from './components/Categories/Categories';
import Builder from'./components/Builder/Builder';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/" exact component={LandingPage} />
          <Route path="/categories" exact component={Categories}/>
          <Route path='/categories/:category'  component={Builder}/>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
